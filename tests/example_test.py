# -*- coding: utf-8 -*-
"""
Example tests to show how we hook up the testing and production code.

1 should fail

Created on Thu Jan 21 07:57:04 2016
@author: laurence
"""

from nose.tools import assert_almost_equal
from lib import example_funcs

def test_my_square_func():
    """
    Smoke test so that we know we've laid-out the project correctly

    Parameters
    ----------
    None

    Returns
    -------
    None

    Side Effects
    ------------
    Raises an assertion error if we do not get the result we expect from
    `my_square_func`
    """
    got = example_funcs.my_square_func(3)
    expected = 9
    assert_almost_equal(got, expected)


def failing_test_of_my_square_func():
    """
    Smoke test so that we know we've laid-out the project correctly

    Parameters
    ----------
    None

    Returns
    -------
    None

    Side Effects
    ------------
    Raises an assertion error if we do not get the result we expect from
    `my_square_func`
    """
    got = example_funcs.my_square_func(-7)
    expected = -49
    assert_almost_equal(got, expected)
