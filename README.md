# Why use objects?

I find a lot of my work involves reading several sets of similar data from
differently formatted files and comparing them.
coefficients
It is easy enough to write a translator between `coefficients.format_a` and
`coefficients.format_b`, but add in a few more formats and this quickly becomes
unwieldy: we would need many functions:

1. `format_a` `->` `format_b`
2. `format_a` `->` `format_c`
3. `format_b` `->` `format_a`
4. `format_b` `->` `format_c`  
5. `format_c` `->` `format_a`
6. `format_c` `->` `format_b`

Moreover, translating 3 different file-types into `*.format_q` then
reading in all the `.format_q` files and do the comparisons is a pain.

Using objects allows you to read data in whatever format but to maintain a
consistent internal representation of the data so comparison is trivial as well as
mutating data from one format to another.

Objects are really just a bunch of data that go around together and also carry
along functions that operate on those data.

They allow us model things in a computer that is closer to the way we tend
to think of things in everyday life. Composition and *'verb-ing'* both make
sense:

> a motorbike *has* an engine and *has* breaks
>
> we can *start* the engine and *apply* the breaks

By using objects we can make our code simpler to understand a use.
Without objects we might need to write something like

```fortran
call utm_to_lat_lon(utm_grid, n_x, n_y, zone_id, lat_out, lon_out, spheroid_out)
```
to transform a grid of UTM co-ordinates to latitude and longitude. It is hard to
remember all the different arguments and what order they are in.

With objects the `utm_grid` carries around most of that information *inside* it,
and the callable 'knows' many of the arguments without us having to pass them in:

```python
lat_lon = utm_grid.to_lat_lon(spheroid_out)
```


Whilst these materials are in `python` the idea of objects is common to many
programming languages. Using `python` means there is a low barrier to entry: but
we could equally do this course in `c++`, `java`, `matlab`, or even
(modern) `fortran`.

## how do we use objects?
Bring up a python terminal (there is one launched as part of the Spyder IDE)
and type:

```python
import numpy as np
myarr = np.array([[1, 2, 3], [4, 5, 6]])
print(myarr)
print(type(myarr))
```
what happened?

what happens when you type
```python
myarr.
```
then hit the `<tab>` key?

what do you see if you do
```python
print(myarr.shape)
print(myarr.size)
```

what about
```python
print(myarr.transpose())
```
why might there be extra `()` after `transpose` but not after `shape`

what happens without the brackets
```python
print(myarr.transpose)
```
___

what happens with
```python
print(transpose(myarr))
```
and

```python
print(np.transpose(myarr))
```
what might be the advantages and disadvantages of `myarr.transpose()` vs. `np.transpose(myarr)`

____

can you predict the results of
```python
print(myarr.transpose().shape)
print(myarr.transpose().size)
```

what about
```python
print(myarr.shape.transpose())
```

> we've created an `array` object
>
> we saw that it knew its `size` and `shape`
>
> and that we could perform 'functions' on it
>
> all accessed by the `.` dot notation

___

## creating our own objects

Objects in python are simply defined like:

```python
class InternetMemeStudio:
    pass

```
copy and paste that code into the `.py` file within the Spyder IDE.
Hit `F5` to run the file; you will probably be asked to save it somewhere 1st.
Running tells the python interpreter about our class. We can then do:

```python
studio = InternetMemeStudio()
```

to create an 'instance' of our class, notice we need brackets `()`.

What happens when you run each of the following 3 lines:

```python
print(studio)
print(InternetMemeStudio)
print(InternetMemeStudio())
```

getting the in-memory address of the instance is not super helpful. Lets
make our class a bit more useful


```python
class InternetMemeStudio:
    def __init__(self, num_kitties):
        self.kitties = num_kitties

    def __repr__(self):
        outstring = 'InternetMemeStudio(num_kitties={0})'.format(self.kitties)
        return outstring
```

what happens if you do
```python
studio = InternetMemeStudio(12)
print(studio.kitties)
```

What happens at the console if you type `kitties.` and hit the `<TAB>` key?

What do you get when you run `print(studio)`? What happens if you copy the output and run it?

What do you think `__init__` and `__repr__` do?

Lets add some functionality to our class.

```python
class InternetMemeStudio:
    def __init__(self, num_kitties):
        self.kitties = num_kitties
        self.tins_per_kittie_per_day = 2
        self.pounds_per_tin = 0.95

    def __repr__(self):
        outstring = 'InternetMemeStudio(num_kitties={0})'.format(self.kitties)
        return outstring

    def buy_a_kittie(self):
        self.kitties = self.kitties + 1

    def daily_food_bill(self):
        total_tins = self.kitties * self.tins_per_kittie_per_day
        cost = total_tins * self.pounds_per_tin
        return cost
```

What do you expect to get from running:

```python
studio = InternetMemeStudio(num_kitties=1)
print(studio.daily_food_bill)
```

What does `self` refer to in `def daily_food_bill(self):` and `cost = total_tins * self.pounds_per_tin`?

### a nearly production ready class
Take a look at the code in `my_1st_class.py`. This is much more like the code we
want to write for real tasks: it contains comments and examples and has constants
kept separately and identified by `CAPITALIZED_NAMES`.

Instantiate the class and see what you can do with it: take a look at the examples for some ideas.

The comments in `my_1st_class.py` suggest a range for latitudes, what happens if
you try to instantiate the class with a `latitude` outwith the stated range?

what happens if you run this in a console:

```python
assert 12 < 7, 'number must be within range'
```

Add a function `my_1st_class.py` to range-check the supplied `latitude` and `longitude`? Where should it be called from (hint: think about what is
happening in `__init__`). Bonus points for not burying hard-coded constants
in the middle of the code.

## project
In the `./tests/data` directory, there are 2 files: `data.spam` and
`results.ham`. They both contain 3D location data.

I'm going to describe what we want our code to do with these files. The
specification takes this form.

#### spec
I want to be able to:
1. to print the contents of `spam` format file in `ham` format
    * and vice versa
2. take the dot products of the data from `spam` and `ham`
    * answer must be (1.0, 10.0, 0.00000001) to within float accuracy
3. to print the y component like `print(spam.y)` and get (0.0,0.0,1.0)

we're going to make a python project that meets these specifications.

### getting started with the project skeleton

If you start a command line in the same directory where you found this
`README.md` and run `nosetests` you should see the results of some examples tests
and production code.

Have a look at the code in `./lib/example_funcs.py` and
`./tests/example_test.py`.

Make sure all the tests pass: `nosetests` should
give you a reassuring line of dots `......` and an `OK`.

What answer would you expect from a function call like `two_to_the_power(3)`?

Write a test for this and run it.

Now write the function so that the test passes.

### project

There are many ways of writing code that satisfies our criteria. I'd suggest
you start by translating part of the [spec's tests](#spec) into tests in code.

You will probably have to use a search engine to help you find how to do things
along the way. Good starting points for searches might be
'Class Objects python', 'numpy read csv', 'pandas read csv'.
If you have a python library installed you can get information about it by
typing something like this at a python console.

```python
import pandas
help(pandas)
```
We will be discussing the code you write at the end of the project
There are people around to help you, you will probably want to get their advice
on the coding decisions you make and the style you write in earlier rather than
later.

Good luck.
