# -*- coding: utf-8 -*-
"""
Created on Tue Jan 26 17:36:22 2016

@author: laurence
"""
import math

ALLOWED_SPHEROIDS = ['WGS84']
DEFAULT_VAL = 0.0
FILL_VALUE = math.nan


class MyLatLon:
    """
    A location defined by latitude longitude and the spheroid they lie on.

    Parameters
    ----------
    latitude : float
        angle from the equator in degrees [-90, 90]

    longitude : float
        angle from the prime meridian in degrees [-180, 180]

    spheroid : str
        which Earth-model are latitude and longitude based on e.g.
        'WGS84'

    Returns
    -------
    A new `MyLatLon` instance

    Examples
    --------
    >>> spam = MyLatLon(latitude=55.6, longitude=-3.0, spheroid='WGS84')
    >>> spam.latitude
    55.6
    >>> spam.longitude
    -3.0
    >>> spam.spheroid
    'WGS84'

    """
    def __init__(self, latitude=DEFAULT_VAL, longitude=DEFAULT_VAL,
                 spheroid=ALLOWED_SPHEROIDS[0]):

        self.latitude = latitude
        self.longitude = longitude
        self.spheroid = spheroid

    def __repr__(self):
        """
        string representing the state of the `MyLatLon` object
        """
        rep = 'MyLatLon(latitude={0}, longitude={1}, spheroid=\'{2}\')'.format(
            self.latitude, self.longitude, self.spheroid)
        return rep

    def change_spheroids(self, new_spheroid=ALLOWED_SPHEROIDS[0]):
        """
        Transform latitude and lingitude to a different spheroid

        Parameters
        ----------

        new_spheroid : str
            which Earth-model are latitude and longitude based on e.g.
            {0}

        Returns
        -------
        A new `MyLatLon` instance

        Examples
        --------
        >>> spam = MyLatLon(latitude=0.0, longitude=0.0, spheroid='WGS84')
        >>> spam.change_spheroids('INT24')
        MyLatLon(latitude=0.0, longitude=0.0, spheroid='INT24')
        """.format(ALLOWED_SPHEROIDS[0])
        # TODO: do maths here to actually do the transformation
        # new_lat, new_lon = transform(self.latitude, self.longitude
        #                              self.spheroid, new_spheroid)
        new_lat = FILL_VALUE
        new_lon = FILL_VALUE
        return MyLatLon(latitude=new_lat,
                        longitude=new_lon,
                        spheroid=new_spheroid)

    def colatitude(self):
        """
        like latitude but north pole is 0 point
        rather than equator

        Returns
        -------
        Colatitude float

        Examples
        --------
        >>> spam = MyLatLon(latitude=56.0, longitude=0.0, spheroid='WGS84')
        >>> spam.colatitude()
        34.0
        """
        return 90.0 - self.latitude
