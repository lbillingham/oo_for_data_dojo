# -*- coding: utf-8 -*-
"""
Created on Thu Jan 21 08:08:56 2016

@author: laurence
"""
import doctest

def my_square_func(a_number):
    """
    Smoke test so that we know we've laid-out the project correctly

    Parameters
    ----------
    a_number : any numeric type
        the number whose squaare you wish to know

    Returns
    -------
    squared :
        the `a_number` squared

    Examples
    --------
    >>> my_square_func(4)
    16

    Side Effects
    ------------
    None
    """
    squared = a_number ** 2
    return squared

if __name__ == '__main__':
    doctest.testmod()